function generateDates(start, daysAgo) {
  let arr = [];

  for (let i = 0; i < daysAgo; i++) {
    const startDate = new Date(start);
    const currentDate = startDate.setDate(new Date(start).getDate() - i);
    const formattedDate = formatDate(currentDate);
    arr.push(formattedDate);
  }

  return arr;
}

function formatDate(date) {
  return new Date(date).toLocaleDateString("en-GB");
}
