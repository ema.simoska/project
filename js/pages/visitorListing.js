const publishedArrItems = items.filter(function (item) {
  return item.isPublished === true;
});

const itemListing = document.querySelector(".itemListing");
const cardDiv = document.querySelectorAll(".card");

publishedArrItems.forEach((item) => {
  if (item.id % 2 === 0) {
    itemListing.innerHTML += `<div class="card">
  <img src="${item.image}"/>
  <div class="content bg-dark">
  <h4>${item.artist}</h4>
  <span>$${item.price}</span>
  <p class="title">${item.title}</p>
  <p class="description">${item.description}</p> </div> </div>`;
  } else {
    itemListing.innerHTML += `<div class="card ">
  <img src="${item.image}"/>
  <div class="content bg-light">
  <h4>${item.artist}</h4>
  <span>$${item.price}</span>
  <p class="title">${item.title}</p>
  <p class="description">${item.description}</p> </div> </div>`;
  }
});

const artistFilter = document.querySelector("#artistFilter");

fetch("https://jsonplaceholder.typicode.com/users")
  .then((res) => res.json())
  .then(function (allUsers) {
    allUsers.forEach(function ({ name }) {
      let option = document.createElement("option");
      option.setAttribute("value", `${name}`);
      option.classList.add("userOption");
      option.innerText = `${name}`;
      artistFilter.append(option);
    });
  });

const filtersIcon = document.querySelector("#filtersIcon");
const filters = document.querySelector(".filters");
const closeIcon = document.querySelector("#closeIcon");
const submitFilterBtn = document.querySelector("#submitFilterBtn");
const filterTitle = document.querySelector("#filterTitle");
const minPrice = document.querySelector("#minPrice");
const maxPrice = document.querySelector("#maxPrice");
const typeFilter = document.querySelector("#typeFilter");

filtersIcon.addEventListener("click", () => {
  filters.style.left = "0";
});

closeIcon.addEventListener("click", () => {
  filters.style.left = "-900px";
});

submitFilterBtn.addEventListener("click", () => {
  filters.style.left = "-900px";
  // itemListing.innerHTML = "";

  const title = filterTitle.value;
  const artist = artistFilter.value;
  const minprice = minPrice.value;
  const maxprice = maxPrice.value;
  const type = typeFilter.value;

  // const title = "Meet me";
  // const artist = 'Ervin Howell';
  // const minprice = 200;
  // const maxprice = 2000;
  // const type = 'painting';

  const filtered = publishedArrItems.filter((item) => {
      (title ? item.title.includes(title) : true) &&
      (artist ? item.artist === artist : true) &&
      (minprice ? item.price >= minprice : true) &&
      (maxprice ? item.price <= maxprice : true) &&
      (type ? item.type === type : true)
    }
  );

  console.log(filtered);
  console.log(title);
  console.log(artist);
  console.log(minprice);
  console.log(maxprice);
  console.log(type);

  // itemListing.innerHTML += `<div class="card ">
  //       <img src="${item.image}"/>
  //       <div class="content bg-light">
  //       <h4>${item.artist}</h4>
  //       <span>$${item.price}</span>
  //       <p class="title">${item.title}</p>
  //       <p class="description">${item.description}</p> </div> </div>`;

  // }
});
