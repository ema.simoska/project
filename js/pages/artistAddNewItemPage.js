const snapshotBtn = document.querySelector("#snapshot");

snapshotBtn.addEventListener("click", () => {
  window.location.href = "#captureImagePopup";
});

const addNewItemForm = document.querySelector("#addNewItemForm");
const titleInput = document.querySelector("#title");
const descriptionInput = document.querySelector("#description");
const typeInput = document.querySelector("#type");
const priceInput = document.querySelector("#price");

const addNewBtn = document.querySelector("#addNewItemBtn");

const artistItems = items.filter(function (item) {
  return item.artist === currentArtist;
});

addNewItemForm.addEventListener("submit", function (e) {
  e.preventDefault();

  let todayDate = new Date().toISOString().slice(0, 10);


  const newItem = {
    title: titleInput.value,
    description: descriptionInput.value,
    type: typeInput.value,
    image: imageData,
    price: priceInput.value,
    artist: currentArtist,
    dateCreated: todayDate,
  };

  artistItems.push(newItem);

  console.log(artistItems);

  document.querySelector(".currentArtistItems").innerHTML += "";

  artistItems.forEach((item) => {
    document.querySelector(
      ".currentArtistItems"
    ).innerHTML += `<div class="artistItem">
      <img src="${item.image}" />
      <div class="content">
      <h4>${item.title}</h4>
      <span class="price">$${item.price}</span>
      <small>${item.dateCreated}</small>
      <p class="description">${item.description}</p>
      </div>
      <div class="buttons">
      <button id="sendToAuction">Send to Auction</button>
      <button id="unpublish">Unpublish</button>
      <button class="remove">Remove</button>
      <button id="edit">Edit</button>
      </div>
      </div>`;
  });

  window.location.hash = "#artistItemPage";
});
