function initArtistHomePage() {
  const canvas = document.querySelector("#myCanvas");
  const dateLabels = generateDates(new Date(), 7);

  const data = {
    labels: dateLabels,
    datasets: [
      {
        label: "Amount",
        backgroundColor: "rgb(161, 106, 94)",
        borderColor: "rgb(161, 106, 94)",
        data: [0, 10, 5, 2, 20, 30, 45],
        barThickness: 10,
      },
    ],
  };

  const config = {
    type: "bar",
    data: data,
    options: {
      indexAxis: "y",
    },
  };

  const myChart = new Chart(canvas, config);

  const last7 = document.querySelector("#last7");
  const last14 = document.querySelector("#last14");
  const last30 = document.querySelector("#last30");

  const filteredItems = items.filter(
    (item) => item.artist === "Leanne Graham" && item.priceSold
  );

  last7.addEventListener("click", function () {
    console.log(myChart);
    myChart.data.datasets[0].barThickness = 10;

    const newLabels = generateDates(new Date(), 7);
    myChart.data.labels = newLabels;

    const newData = newLabels.map((label) => {
      let sum = 0;
      filteredItems.forEach((item) => {
        if (formatDate(item.dateSold) === label) {
          sum += item.priceSold;
        }
      });
      return sum;
    });

    myChart.data.datasets[0].data = newData;
    myChart.update();
  });

  last14.addEventListener("click", function () {
    console.log(myChart);

    const newLabels = generateDates(new Date(), 14);
    myChart.data.labels = newLabels;

    const newData = newLabels.map((label) => {
      let sum = 0;
      filteredItems.forEach((item) => {
        if (formatDate(item.dateSold) === label) {
          sum += item.priceSold;
        }
      });
      return sum;
    });

    myChart.data.datasets[0].data = newData;
    myChart.update();
  });

  last30.addEventListener("click", function () {
    console.log(myChart);

    myChart.data.labels = generateDates(new Date(), 30);
    myChart.data.datasets[0].barThickness = 2;
    myChart.data.datasets[0].data = [
      0, 10, 5, 2, 20, 30, 45, 0, 10, 5, 2, 20, 30, 45, 0, 10, 5, 2, 20, 30, 45,
      0, 10, 5, 2, 20, 30, 45, 90, 110,
    ];
    myChart.update();
  });

  
}
