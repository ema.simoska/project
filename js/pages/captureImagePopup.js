let imageData;
function initCaptureImagePopup() {
  const captureImageBtn = document.querySelector("#captureImage");
  const snapshotDiv = document.querySelector("#snapshot");
  let video = document.querySelector("#video");
  let canvas = document.querySelector("#canvas");
  let image = document.querySelector("#image");
  let allVideoDevices;
  let width = 390;
  let height = 400;

  navigator.mediaDevices.enumerateDevices().then((data) => {
    allVideoDevices = data.filter((x) => x.kind === "videoinput");
  });

  function initCamera() {
    const constrains = {
      audio: false,
      video: {
        facingMode: { ideal: "environment" },
      },
    };
    navigator.mediaDevices.getUserMedia(constrains).then((stream) => {
      video.srcObject = stream;
    });
    video.addEventListener("canplay", function () {
      video.setAttribute("width", width);
      video.setAttribute("height", height);
      canvas.setAttribute("width", width);
      canvas.setAttribute("height", height);
    });

    captureImageBtn.addEventListener("click", captureImage);
  }

  function captureImage() {
    const context = canvas.getContext("2d");

    context.drawImage(video, 0, 0, width, height);

    imageData = canvas.toDataURL("image/png");

    image.src = imageData;

    
    snapshotDiv.innerHTML = "";
    
    snapshotDiv.innerHTML += `<img src="${imageData}"></img>`;
    location.hash = "#artistAddNewItemPage";
  }

  initCamera();
}
