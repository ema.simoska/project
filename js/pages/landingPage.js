let usersSelect = document.querySelector("#usersSelect");
let currentArtist;
let artistName = document.querySelectorAll(".artistName");
const totalItems = document.querySelector("#totalItems");
const soldItems = document.querySelector("#soldItems");
const totalIncome = document.querySelector("#totalIncome");
// let artistNameItemPage = document.querySelector("#artistNameItemPage");

// function initLandingPage() {
//   artistName.innerText = "";

//   getUsers();

// }

function onUserChange(e) {
  currentArtist = e.currentTarget.value;
  localStorage.setItem("activeUser", currentArtist);

  artistName.forEach((x) => {
    x.innerHTML += currentArtist;
  });
  // artistNameItemPage.innerHTML += e.currentTarget.value;
  window.location = "#artistHomePage";

  const artistItems = items.filter(function (item) {
    return item.artist === currentArtist;
  });

  // console.log(currentArtist);

  artistItems.forEach((item) => {
    document.querySelector(
      ".currentArtistItems"
    ).innerHTML += `<div class="artistItem">
      <img src="${item.image}" />
      <div class="content">
      <h4>${item.title}</h4>
      <span class="price">$${item.price}</span>
      <small>${item.dateCreated}</small>
      <p class="description">${item.description}</p>
      </div>
      <div class="buttons">
      <button id="sendToAuction">Send to Auction</button>
      <button id="unpublish">Unpublish</button>
      <button id="remove">Remove</button>
      <button id="edit">Edit</button>
      </div>
      </div>`;
  });

  console.log(artistItems);

  const filteredSold = artistItems.filter(function (item) {
    return item.dateSold;
  });

  filteredPublished = artistItems.filter(function (item) {
    return item.isPublished === true;
  });

  console.log(filteredSold);
  console.log(filteredPublished);

  soldItems.innerText = filteredSold.length;
  totalItems.innerText = filteredPublished.length;

  filteredSold.forEach((item) => {
    let all = item.priceSold;
    console.log(all);
  });
}

function getUsers() {
  fetch("https://jsonplaceholder.typicode.com/users")
    .then((res) => res.json())
    .then(function (allUsers) {
      allUsers.forEach(function ({ name }) {
        let option = document.createElement("option");
        option.setAttribute("value", `${name}`);
        option.classList.add("userOption");
        option.innerText = `${name}`;
        usersSelect.append(option);
      });
    });
}

getUsers();
usersSelect.addEventListener("change", onUserChange);

document.getElementById("joinAsVisitor").addEventListener("click", function () {
  window.location.href = "#visitorHomePage";
});
