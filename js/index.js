window.addEventListener("hashchange", handleRoute);
window.addEventListener("load", handleRoute);

function handleRoute() {
  const hash = location.hash;

  document.querySelectorAll("section").forEach((section) => {
    section.style.display = "none";
  });

  hash && (document.querySelector(hash).style.display = "block");

  switch (hash) {
    case "#landingPage":
      // initLandingPage();
      break;

    case "#visitorHomePage":
      initVisitorHomePage();
      break;

    case "#visitorListing":
      // initVisitorHomePage();
      break;

    case "#artistHomePage":
      initArtistHomePage();
      break;

    case "#auction":
      // initVisitorHomePage();
      break;

    case "#artistItemPage":
      initArtistItemPage();
      break;

    case "#artistAddNewItemPage":
      // initVisitorHomePage();
      break;

    case "#captureImagePopup":
      initCaptureImagePopup();
      break;

    default:
      document.querySelector("#landingPage").style.display = "block";
      break;
  }
}

function menuFunction() {
  const links = document.querySelectorAll(".myLinks");
  links.forEach((x) => {
    if (x.style.display === "block") {
      x.style.display = "none";
    } else {
      x.style.display = "block";
    }
  });
}
